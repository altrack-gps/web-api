## ALTrack API

Steps to setup and run it:
1. Create new virtual environment;
2. Install dependencies from requirements.txt;
3. Create `local.py` from `main/settings/local-example.py`;
4. Install `GDAL`:  
    `sudo apt-get install binutils libproj-dev gdal-bin`
5. Setup `OSRM` (Open Source Route Machine);
6. Run postgresql with PostGIS extension in Docker:
    * `docker pull mdillon/postgis`
    * ```
      docker run --rm --name container-name -e POSTGRES_PASSWORD='postgres' -e POSTGRES_USER='postgres' -d -p 5432:5432 \
      -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data mdillon/postgis
      ```
    * Create database;
    * Enable PostGIS extension: run `psql` from container and run:  
        `CREATE EXTENSION postgis;`
7. Run tests: `python manage.py test`
8. Run server: `python manage.py runserver`
