from django.db import models

from common.querysets import DraftQuerySet


class DraftModel(models.Model):
    draft = models.BooleanField(default=True)

    objects = DraftQuerySet.as_manager()

    class Meta:
        abstract = True
