from django.db.models import QuerySet


class DraftQuerySet(QuerySet):
    def drafts(self):
        return self.filter(draft=True)

    def saved(self):
        return self.filter(draft=False)
