from django.contrib import admin

from trucks.models import Truck


admin.site.register(Truck)
