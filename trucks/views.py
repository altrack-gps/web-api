from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action, api_view
from django_filters.rest_framework import DjangoFilterBackend

from trucks.models import Truck, TruckState, TruckEvent
from trucks.filters import TruckStateFilter, TruckEventFilter, TruckFilter
from trucks.serializers import TruckSerializer, TruckRouteSerializer, TruckStateSerializer, TruckEventSerializer


class TruckViewSet(ModelViewSet):
    queryset = Truck.objects.all()
    serializer_class = TruckSerializer
    filterset_class = TruckFilter
    filter_backends = (DjangoFilterBackend,)

    @action(methods=['put'], detail=True)
    def set_route(self, request, pk):
        serializer = TruckRouteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.update(self.get_object(), serializer.data)
            return Response({'status': 'ok'})
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )

    @action(methods=['get'], detail=False)
    def titles(self, request):
        titles = self.filter_queryset(
            self.get_queryset()
        ).values('id', 'plate_number')

        return Response(titles)


class TruckStateListView(ListAPIView):
    queryset = TruckState.objects.all()
    serializer_class = TruckStateSerializer
    filterset_class = TruckStateFilter
    filter_backends = (DjangoFilterBackend,)

    def get_queryset(self):
        return TruckState.objects.filter(truck_id=self.kwargs['pk'])


class TruckEventListView(ListAPIView):
    queryset = TruckEvent.objects.all()
    serializer_class = TruckEventSerializer
    filterset_class = TruckEventFilter
    filter_backends = (DjangoFilterBackend,)

    def get_queryset(self):
        return TruckEvent.objects\
            .filter(truck_id=self.kwargs['truck_id'])\
            .order_by('event_at')


@api_view(['GET'])
def get_last_truck_event(request, truck_id):
    event = TruckEvent.objects \
        .filter(truck_id=truck_id) \
        .order_by('event_at').last()

    return Response(TruckEventSerializer(event).data)
