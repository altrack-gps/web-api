from rest_framework import serializers

from routes.fields import PointField
from trucks.models import Truck, Route, TruckState, TruckEvent


class TruckSerializer(serializers.ModelSerializer):
    plateNumber = serializers.CharField(source='plate_number')
    routeID = serializers.IntegerField(
        source='route_id',
        read_only=True
    )
    routeTitle = serializers.StringRelatedField(
        source='route',
        read_only=True
    )

    class Meta:
        model = Truck
        fields = ('id', 'imei', 'plateNumber', 'routeID', 'routeTitle')


class TruckRouteSerializer(serializers.ModelSerializer):
    routeID = serializers.PrimaryKeyRelatedField(
        queryset=Route.objects.all(),
        source='route_id',
        allow_null=True
    )

    class Meta:
        model = Truck
        fields = ('routeID',)

    def update(self, instance, validated_data):
        return super().update(instance, {
            'route_id': validated_data['routeID']
        })


class TruckStateSerializer(serializers.ModelSerializer):
    position = PointField()

    class Meta:
        model = TruckState
        fields = '__all__'


class TruckEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = TruckEvent
        fields = '__all__'
