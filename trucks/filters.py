import django_filters as filters

from trucks.models import TruckState, TruckEvent, Truck


class TruckStateFilter(filters.FilterSet):
    state_at = filters.DateTimeFromToRangeFilter()

    class Meta:
        model = TruckState
        fields = ['state_at']


class TruckEventFilter(filters.FilterSet):
    event_at = filters.DateTimeFromToRangeFilter()

    class Meta:
        model = TruckEvent
        fields = ['event_at']


class TruckFilter(filters.FilterSet):
    imei = filters.CharFilter(lookup_expr='contains')
    plate_number = filters.CharFilter(lookup_expr='contains')

    class Meta:
        model = Truck
        fields = ['imei', 'plate_number', 'route']
