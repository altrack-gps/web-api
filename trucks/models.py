from django.contrib.gis.db import models
from django.contrib.postgres.fields import JSONField

from routes.models import Route
from trucks.enums import TruckEventType


class Truck(models.Model):
    MAX_IMEI_LENGTH = 16

    imei = models.CharField(
        max_length=MAX_IMEI_LENGTH,
        unique=True
    )
    plate_number = models.CharField(
        max_length=8,
        unique=True
    )
    route = models.ForeignKey(
        Route,
        null=True,
        on_delete=models.SET_NULL
    )

    class Meta:
        db_table = 'trucks'

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.imei = self.imei.zfill(self.MAX_IMEI_LENGTH)
        return super().save(force_insert, force_update, using, update_fields)

    def __str__(self):
        return self.plate_number


class TruckState(models.Model):
    truck = models.ForeignKey('Truck', on_delete=models.DO_NOTHING)
    course = models.IntegerField()
    speed = models.IntegerField()
    state_at = models.DateTimeField()
    position = models.PointField(geography=True)

    class Meta:
        db_table = 'truck_states'


class TruckEvent(models.Model):
    type = models.IntegerField(choices=TruckEventType.choices)
    event_at = models.DateTimeField()
    payload = JSONField(max_length=150, default=dict)
    truck = models.ForeignKey(Truck, on_delete=models.PROTECT)

    class Meta:
        db_table = 'truck_events'
