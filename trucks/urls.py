from django.conf.urls import url
from rest_framework.routers import DefaultRouter

from trucks.views import TruckViewSet, TruckEventListView, TruckStateListView, get_last_truck_event

router = DefaultRouter()
router.register(r'trucks', TruckViewSet)


urlpatterns = [
    url(r'truck-history/(?P<pk>\d+)$', TruckStateListView.as_view(), name='truck-history'),
    url(r'truck-events/(?P<truck_id>\d+)/last', get_last_truck_event, name='truck-events-last'),
    url(r'truck-events/(?P<truck_id>\d+)/', TruckEventListView.as_view(), name='truck-events'),
]
