from django.test import TestCase
from django.utils import timezone

from trucks.enums import TruckEventType
from trucks.models import TruckEvent
from trucks.tests import factories


class TestTruckEventModel(TestCase):
    def test_create(self):
        truck = factories.TruckFactory()
        event = TruckEvent.objects.create(
            type=TruckEventType.LEFT_ROUTE,
            truck=truck,
            event_at=timezone.now()
        )
        self.assertEqual(event.type, TruckEventType.LEFT_ROUTE)
