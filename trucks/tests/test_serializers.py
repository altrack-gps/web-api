from django.test import TestCase

from trucks.models import Truck
from trucks.serializers import TruckSerializer, TruckRouteSerializer
from trucks.tests import factories

from routes.tests import factories as route_factories


class TestTruckSerializer(TestCase):
    maxDiff = None

    def test_serialization(self):
        truck = factories.TruckFactory()
        serialized_data = {
            'id': truck.id,
            'imei': truck.imei,
            'plateNumber': truck.plate_number,
            'routeID': truck.route_id,
            'routeTitle': truck.route.title
        }
        serializer = TruckSerializer(truck)

        self.assertDictEqual(
            serializer.data,
            serialized_data
        )

    def test_deserialization(self):
        truck_json = {
            'imei': '5445454'.zfill(Truck.MAX_IMEI_LENGTH),
            'plateNumber': 'AA3443AA',
            'routeID': None
        }

        serializer = TruckSerializer(data=truck_json)
        serializer.is_valid(raise_exception=True)
        truck = serializer.save()

        self.assertEqual(truck.imei, truck_json['imei'])
        self.assertEqual(truck.plate_number, truck_json['plateNumber'])
        self.assertEqual(truck.route_id, truck_json['routeID'])


class TestTruckRouteSerializer(TestCase):
    def test_update(self):
        truck = factories.TruckFactory()
        route = route_factories.RouteFactory()
        data = {
            'routeID': route.id
        }
        serializer = TruckRouteSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.update(truck, data)

        self.assertEqual(
            truck.route_id,
            route.id
        )
