import string

import factory
from factory import fuzzy

from routes.tests import factories
from trucks import models


class TruckFactory(factory.DjangoModelFactory):
    plate_number = fuzzy.FuzzyText(
        length=4,
        prefix='AA',
        suffix='DD',
        chars=list(string.digits)
    )
    imei = fuzzy.FuzzyText(
        length=16,
        chars=list(string.digits)
    )
    route = factory.SubFactory(factories.RouteFactory)

    class Meta:
        model = models.Truck
