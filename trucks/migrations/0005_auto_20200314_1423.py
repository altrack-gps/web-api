
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trucks', '0004_auto_20190521_1803'),
    ]

    operations = [
        migrations.AlterField(
            model_name='truckevent',
            name='type',
            field=models.IntegerField(choices=[(1, 'LEFT_ROUTE'), (2, 'START_ROUTE'), (3, 'FINISH_ROUTE'), (4, 'MOVE_ON_ROUTE'), (5, 'PASSING_WAY_POINT')]),
        ),
    ]
