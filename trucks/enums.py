from django.db import models
from django.utils.translation import gettext_lazy as _


class TruckEventType(models.IntegerChoices):
    LEFT_ROUTE = 1, _('LEFT_ROUTE')
    START_ROUTE = 2, _('START_ROUTE')
    FINISH_ROUTE = 3, _('FINISH_ROUTE')
    MOVE_ON_ROUTE = 4, _('MOVE_ON_ROUTE')
    PASSING_WAY_POINT = 5, _('PASSING_WAY_POINT')
