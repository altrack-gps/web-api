from shared.router import DefaultRouter

from trucks.urls import router as truck_router, urlpatterns as truck_patterns
from routes.urls import router as route_router
from reports.urls import router as report_router


router = DefaultRouter()
router.extend(truck_router)
router.extend(route_router)
router.extend(report_router)


urlpatterns = [
    *router.urls,
    *truck_patterns
]
