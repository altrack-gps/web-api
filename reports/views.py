from django.http import JsonResponse
from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet
from django_filters.rest_framework import DjangoFilterBackend

from reports.models import TruckReport
from reports.filters import TruckReportFilter
from reports.services import TruckStats
from reports.serializers import TruckReportSerializer


class TruckReportViewSet(ModelViewSet):
    model = TruckReport
    queryset = TruckReport.objects.all().order_by('start_date')
    serializer_class = TruckReportSerializer
    filter_backends = (DjangoFilterBackend, )
    filter_class = TruckReportFilter

    http_method_names = ['get']

    def get_queryset(self):
        qs = self.filter_queryset(self.queryset)
        params = self.request.query_params

        if 'finished' in params:
            qs = qs.finished()
        elif 'unfinished' in params:
            qs = qs.active()

        return qs

    @action(methods=['get'],
            detail=False,
            url_name='finished_routes',
            url_path='finished-routes/(?P<truck_id>[0-9]+)')
    def finished_routes(self, request, truck_id):
        year = self.request.query_params.get('year')
        stats = TruckStats.get_per_year(truck_id, year)

        return JsonResponse(stats, safe=False)
