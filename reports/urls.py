from rest_framework.routers import DefaultRouter

from reports.views import TruckReportViewSet


router = DefaultRouter()
router.register(r'truck-reports', TruckReportViewSet)
