from django.db import models
from django.utils import timezone

from reports.querysets import TruckReportQuerySet
from routes.models import Route
from trucks.models import Truck


class TruckReport(models.Model):
    truck = models.ForeignKey(Truck, on_delete=models.PROTECT)
    route = models.ForeignKey(Route, on_delete=models.PROTECT)
    start_date = models.DateTimeField(null=True, default=timezone.now)
    finish_date = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = 'truck_reports'

    objects = TruckReportQuerySet.as_manager()
