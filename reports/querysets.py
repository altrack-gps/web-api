from django.db.models import QuerySet


class TruckReportQuerySet(QuerySet):
    def finished(self):
        return self.filter(finish_date__isnull=False)

    def active(self):
        return self.filter(finish_date__isnull=True)
