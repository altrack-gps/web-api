import django_filters as filters

from reports.models import TruckReport


class TruckReportFilter(filters.FilterSet):
    start_date = filters.DateFromToRangeFilter()
    finish_date = filters.DateFromToRangeFilter()

    class Meta:
        model = TruckReport
        fields = ('truck', 'route', 'start_date', 'finish_date')
