import calendar

from datetime import datetime
from typing import List, Dict

from django.db.models import Count
from django.db.models.functions import ExtractMonth

from reports.models import TruckReport


MONTHS = 12


class TruckStats:
    @classmethod
    def get_per_year(cls,
                     truck_id: int,
                     year: int = None) -> Dict:
        if not year:
            year = datetime.now().year

        stats = TruckReport.objects.all()\
            .order_by('start_date')\
            .filter(truck_id=truck_id, finish_date__year=year)\
            .finished()\
            .annotate(month=ExtractMonth('finish_date'))\
            .values('month')\
            .annotate(number_of_routes=Count('id'))\
            .order_by()\
            .values('month', 'number_of_routes')
        return cls._to_calendar(stats)

    @classmethod
    def _to_calendar(cls, stats: List[Dict]) -> Dict:
        per_month = {
            s['month']: s['number_of_routes']
            for s in stats
        }
        calendar_stats = {
            calendar.month_name[m_num + 1]: per_month[m_num + 1] if per_month.get(m_num + 1) else 0
            for m_num in range(MONTHS)
        }
        return calendar_stats
