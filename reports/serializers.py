from rest_framework import serializers

from reports.models import TruckReport


class TruckReportSerializer(serializers.ModelSerializer):
    truckTitle = serializers.StringRelatedField(
        source='truck',
        read_only=True
    )

    routeTitle = serializers.StringRelatedField(
        source='route',
        read_only=True
    )

    class Meta:
        model = TruckReport
        fields = '__all__'
