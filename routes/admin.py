from django.contrib import admin

from routes.models import Address, WayPoint, Route


admin.site.register(Route)
admin.site.register(WayPoint)
admin.site.register(Address)
