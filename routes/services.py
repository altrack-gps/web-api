from routes.models import WayPoint


class WayPointService:
    @classmethod
    def get_all_by_route(cls, route_id: int):
        return WayPoint.objects.filter(route_id=route_id)
