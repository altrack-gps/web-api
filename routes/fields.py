from rest_framework import serializers
from django.contrib.gis.geos import Point, LineString


class PointField(serializers.Field):
    def to_representation(self, value):
        return {
            'lat': value.coords[0],
            'lng': value.coords[1]
        }

    def to_internal_value(self, data):
        return Point(data['lat'], data['lng'])


class LineField(serializers.Field):
    def to_representation(self, value):
        return value.coords

    def to_internal_value(self, data):
        return LineString(data)
