from enum import Enum

from django.contrib.gis.db import models

from common.models import DraftModel
from routes.querysets import RouteQuerySet


class Route(DraftModel):
    title = models.CharField(
        max_length=50,
        unique=True
    )
    duration = models.DurationField()
    distance = models.FloatField()
    line = models.LineStringField(geography=True)

    objects = RouteQuerySet.as_manager()

    class Meta:
        db_table = 'routes'

    def __str__(self):
        return self.title


class WayPoint(models.Model):
    position = models.PointField(geography=True)
    number = models.IntegerField()
    route = models.ForeignKey('Route', on_delete=models.CASCADE)
    address = models.OneToOneField('Address', on_delete=models.CASCADE)
    # TODO after route has been deleted but addresses hasn't

    class Meta:
        db_table = 'waypoints'
        unique_together = ('route', 'number')


class LocalityTypes(Enum):
    CITY = 'CITY'
    VILLAGE = 'VILLAGE'


class Address(models.Model):
    house_number = models.CharField(
        max_length=5,
        null=True
    )
    road = models.CharField(
        max_length=50,
        null=True
    )
    locality = models.CharField(
        max_length=50,
        null=True
    )
    locality_type = models.CharField(
        max_length=15,
        choices=[
            (type.value, type.value) for type in LocalityTypes
        ],
        null=True
    )
    state = models.CharField(
        max_length=50,
        null=True
    )
    country = models.CharField(max_length=50)

    class Meta:
        db_table = 'waypoint_addresses'
