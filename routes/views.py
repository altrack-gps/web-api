from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from django_filters.rest_framework import DjangoFilterBackend

from routes.models import Route
from routes.filters import RouteFilter
from routes.serializers import RouteSerializer, RouteAddressSerializer


class RouteViewSet(ModelViewSet):
    queryset = Route.objects.all()
    serializer_class = RouteSerializer
    filter_backends = (DjangoFilterBackend, )
    filter_class = RouteFilter

    @action(methods=['get'], detail=False)
    def titles(self, *_):
        titles = self.filter_queryset(
            self.get_queryset()
        ).values('id', 'title')

        return Response(titles)

    @action(methods=['get'], detail=False)
    def with_addresses(self, request):
        route_title = request.query_params.get('title')
        if route_title:
            routes = Route.objects.filter(title__icontains=route_title)
        else:
            routes = Route.objects.all()

        serializer = RouteAddressSerializer(routes.with_addresses(), many=True)
        return Response(serializer.data)
