from django.apps import apps
from django.db.models import Min, Max

from common.querysets import DraftQuerySet


class RouteQuerySet(DraftQuerySet):
    def with_addresses(self):
        WayPoint = apps.get_model('routes', 'WayPoint')
        Address = apps.get_model('routes', 'Address')

        routes = self.all()

        for r in routes:
            points = WayPoint.objects.filter(route_id=r.id)\
                .aggregate(
                    start_num=Min('number'),
                    stop_num=Max('number')
                )
            r.start_address = Address.objects.get(
                waypoint__number=points['start_num'],
                waypoint__route__id=r.id
            )
            r.stop_address = Address.objects.get(
                waypoint__number=points['stop_num'],
                waypoint__route__id=r.id
            )

        return routes
