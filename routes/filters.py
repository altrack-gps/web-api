import django_filters as filters

from routes.models import Route


class RouteFilter(filters.FilterSet):
    title = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Route
        fields = ('title',)
