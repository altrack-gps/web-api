from rest_framework.routers import DefaultRouter

from routes.views import RouteViewSet


router = DefaultRouter()
router.register(r'routes', RouteViewSet)
