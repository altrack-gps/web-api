from rest_framework import serializers
from django.db.transaction import atomic

from routes.fields import PointField, LineField
from routes.models import Address, WayPoint, Route


class AddressSerializer(serializers.ModelSerializer):
    localityType = serializers.CharField(source='locality_type')
    houseNumber = serializers.CharField(
        source='house_number',
        required=False,
        allow_blank=True,
        allow_null=True
    )

    class Meta:
        model = Address
        fields = ('houseNumber', 'road', 'locality', 'localityType', 'state', 'country')


class WayPointSerializer(serializers.ModelSerializer):
    route_id = serializers.IntegerField(
        required=False,
        write_only=True
    )
    position = PointField()
    address = AddressSerializer()

    class Meta:
        model = WayPoint
        fields = ('route_id', 'position', 'address', 'number')

    def create(self, validated_data):
        address = AddressSerializer().create(
            validated_data.pop('address')
        )
        validated_data['address'] = address
        return super().create(validated_data)


class RouteSerializer(serializers.ModelSerializer):
    line = LineField()
    wayPoints = WayPointSerializer(
        source='waypoint_set',
        many=True
    )

    class Meta:
        model = Route
        fields = ('id', 'title', 'duration', 'distance', 'line', 'wayPoints')

    @atomic
    def create(self, validated_data):
        route = Route.objects.create(
            title=validated_data['title'],
            line=validated_data['line'],
            duration=validated_data['duration'],
            distance=validated_data['distance']
        )

        for wp in validated_data['waypoint_set']:
            wp['route_id'] = route.id
            WayPointSerializer().create(wp)

        return route

    @atomic
    def update(self, instance, validated_data):
        instance.line = validated_data.get('line', instance.line)
        instance.title = validated_data.get('title', instance.title)
        instance.duration = validated_data.get('duration', instance.duration)
        instance.distance = validated_data.get('distance', instance.distance)

        instance.save()
        instance.waypoint_set.all().delete()

        for wp in validated_data['waypoint_set']:
            wp['route_id'] = instance.id
            WayPointSerializer().create(wp)
        return instance


class RouteAddressSerializer(serializers.ModelSerializer):
    startAddress = AddressSerializer(read_only=True, source='start_address')
    stopAddress = AddressSerializer(read_only=True, source='stop_address')

    class Meta:
        model = Route
        fields = ('id', 'title', 'startAddress', 'stopAddress')

