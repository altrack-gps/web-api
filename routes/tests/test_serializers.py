import json
from datetime import datetime

from django.test import TestCase

from routes.fields import LineField
from routes.models import LocalityTypes
from routes.serializers import AddressSerializer, WayPointSerializer, RouteSerializer
from routes.tests import factories


class TestAddressSerializer(TestCase):
    def test_serialization(self):
        address = factories.AddressFactory()
        serialized_data = {
            'houseNumber': address.house_number,
            'road': address.road,
            'locality': address.locality,
            'localityType': address.locality_type,
            'state': address.state,
            'country': address.country
        }
        serializer = AddressSerializer(address)

        self.assertDictEqual(
            serializer.data,
            serialized_data
        )

    def test_deserialization(self):
        serialized_data = {
            'houseNumber': '1-a',
            'road': 'test',
            'locality': 'test',
            'localityType': LocalityTypes.CITY.value,
            'state': 'test',
            'country': 'test'
        }
        serializer = AddressSerializer(data=serialized_data)
        serializer.is_valid(raise_exception=True)
        address = serializer.save()

        self.assertDictEqual(
            serialized_data,
            AddressSerializer(address).data
        )


class TestWayPointSerializer(TestCase):
    def test_serialization(self):
        way_point = factories.WayPointFactory()
        serialized_address = AddressSerializer(way_point.address).data
        serialized_data = {
            'number': way_point.number,
            'position': {
                'lat': way_point.position.coords[0],
                'lng': way_point.position.coords[1]
            },
            'address': serialized_address
        }
        serializer = WayPointSerializer(way_point)

        self.assertDictEqual(
            serializer.data,
            serialized_data
        )

    def test_deserialization(self):
        route = factories.RouteFactory()
        serialized_way_point = {
            'route_id': route.id,
            'number': 1,
            'position': {
                'lat': 0.0,
                'lng': 0.0
            },
            'address': {
                'houseNumber': '1-a',
                'road': 'test',
                'locality': 'test',
                'localityType': LocalityTypes.CITY.value,
                'state': 'test',
                'country': 'test'
            }
        }

        serializer = WayPointSerializer(data=serialized_way_point)
        serializer.is_valid(raise_exception=True)
        way_point = serializer.save()

        export_way_point = {
            **serialized_way_point
        }
        export_way_point.pop('route_id')

        self.assertDictEqual(
            export_way_point,
            dict(WayPointSerializer(way_point).data)
        )


class TestRouteSerializer(TestCase):
    def test_serialization(self):
        wp_start = factories.WayPointFactory()
        route = wp_start.route
        wp_end = factories.WayPointFactory(route=route)

        serialized_route = {
            'id': route.id,
            'title': route.title,
            'distance': float(route.distance),
            'duration': _from_timedelta_to_str(route.duration),
            'line': LineField().to_representation(route.line),
            'wayPoints': [
                WayPointSerializer(wp_start).data,
                WayPointSerializer(wp_end).data
            ]
        }
        serializer = RouteSerializer(route)

        self.assertDictEqual(
            json.loads(json.dumps(serialized_route)),
            json.loads(json.dumps(serializer.data))
        )

    def test_deserialization(self):
        route = factories.RouteFactory()
        factories.WayPointFactory.create_batch(size=5, route=route)

        serialized_route = RouteSerializer(route).data
        serialized_route['title'] = 'test'

        serializer = RouteSerializer(data=serialized_route)
        serializer.is_valid(raise_exception=True)
        saved_route = serializer.save()

        self.assertEqual(serialized_route['title'], saved_route.title)

        way_points = route.waypoint_set.order_by('id')
        saved_way_points = saved_route.waypoint_set.order_by('id')
        for i, wp in enumerate(saved_way_points):
            self.assert_way_point(way_points[i], wp)

    def assert_way_point(self, wp, checked):
        self.assertEqual(wp.position, checked.position)
        self.assertEqual(wp.number, checked.number)
        self.assert_address(wp.address, checked.address)

    def assert_address(self, address, checked):
        self.assertEqual(address.house_number, checked.house_number)
        self.assertEqual(address.road, checked.road)
        self.assertEqual(address.locality, checked.locality)
        self.assertEqual(address.locality_type, checked.locality_type)
        self.assertEqual(address.state, checked.state)
        self.assertEqual(address.country, checked.country)


def _from_timedelta_to_str(td):
    return (
        datetime(hour=0, minute=0, second=0, day=1, month=1, year=1970) + td
    ).time().strftime('%H:%M:%S')
