from datetime import timedelta

import factory

from django.contrib.gis.geos import LineString, Point
from factory import fuzzy

from routes import models


class FuzzyLineString(fuzzy.BaseFuzzyAttribute):
    def fuzz(self):
        return LineString(
            [0, 0],
            [0, 0]
        )


class RouteFactory(factory.DjangoModelFactory):
    title = fuzzy.FuzzyText(length=10)
    duration = timedelta()
    distance = fuzzy.FuzzyDecimal(10000)
    line = FuzzyLineString()

    class Meta:
        model = models.Route


class AddressFactory(factory.DjangoModelFactory):
    house_number = fuzzy.FuzzyText(length=5)
    road = fuzzy.FuzzyText(length=10)
    locality = fuzzy.FuzzyText(length=10)
    locality_type = fuzzy.FuzzyChoice([tag.value for tag in models.LocalityTypes])
    state = fuzzy.FuzzyText(length=10)
    country = fuzzy.FuzzyText(length=10)

    class Meta:
        model = models.Address


class WayPointFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.WayPoint

    number = factory.Sequence(lambda n: n)
    address = factory.SubFactory(AddressFactory)
    position = Point(0, 0)
    route = factory.SubFactory(RouteFactory)
