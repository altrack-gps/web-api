from django.test import TestCase
from django.contrib.gis.geos import Point, LineString

from routes.fields import LineField
from trucks.serializers import PointField


class TestPointField(TestCase):
    def test_to_representation(self):
        p = Point(11.11, 22.22)
        field = PointField()

        repr_point = field.to_representation(p)
        self.assertDictEqual(repr_point, {
            'lat': p.coords[0],
            'lng': p.coords[1]
        })

    def test_to_internal_value(self):
        repr_point = {
            'lat': 11.11,
            'lng': 22.22
        }
        point_obj = Point(repr_point['lat'], repr_point['lng'])

        field = PointField()
        converted_point = field.to_internal_value(repr_point)
        self.assertEqual(point_obj, converted_point)


class TestLineField(TestCase):
    def test_to_representation(self):
        line_coordinates = (
            (11, 11),
            (22, 22)
        )
        line = LineString(line_coordinates)
        field = LineField()

        line_repr = field.to_representation(line)
        self.assertTupleEqual(line_coordinates, line_repr)

    def test_to_internal_value(self):
        line_coordinates = (
            (11, 11),
            (22, 22)
        )
        field = LineField()
        line = field.to_internal_value(line_coordinates)
        converted_line = LineString(line_coordinates)

        self.assertEqual(line, converted_line)
